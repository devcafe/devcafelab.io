---
title: Background documentation for decisions about the open source transition of the Dalton/LSDalton code
published: 2017-03-16
author: Radovan Bast
---

# Authors

Radovan Bast and Roberto Di Remigio


# Executive summary

This document presents options for decisions about the open source transition
of the Dalton/LSDalton code and analyses these options according to strengths,
weaknesses, risks, and opportunities.

It is essential to note and understand that with Git by design it is not
possible to serve public and private branches in one single repository. This
means that by making one or more branches public, we need at least two
repositories.

Four separate and numbered decision items are discussed in this document:

- 1) Decision about the repository hosting platform
- 2) Decision about which branches should be part of the public repository.
- 3) Decision about the timeline for opening the public repository
- 4) Decision about the contribution model for the public repository

**TODO** Change in workflow: mention CI, code review and update list of items
**TODO** Stress that choice of hosting platform is tightly coupled to choice of CI service

# Definitions

**TODO** Mention code review

## Repository

- **Public repository** repository that is world-readable and can be cloned anonymously
- **Private repository** only a restricted set of persons can clone the repository

## Branch

**Branch visibility** is the set of _read/write restrictions_ applied to a branch.
We can define three levels:
  - **Public branch** a branch which is read- _and_ write-accessible to any member of the repository
  - **Private branch** a branch which is only read- _and_ write-accessible to limited number of members
  - **Protected branch** a, public _or_ private, branch with **restricted** write access

#### Disambiguation

As noted in the [Executive summary](#executive-summary),
**the privacy setting of a branch is directly inherited from the privacy setting of the repository.**
Thus:
1. A **public repository** can only have public _and_ protected branches.
2. A **private repository** can only have private _and_ protected branches.

## Continuous integration

Running a job building the code and running the test suite (or a subset of it) is called _continuous integration_
- **CI service** the infrastructure and software coordinating the CI jobs with events on the repository


# 1) Decision about the repository hosting platform

## Options

- Option 1.1: [GitHub](https://github.com)
- Option 1.2: [GitLab.com](https://gitlab.com)
- Option 1.3: Nordic-wide GitLab server deployed by [NeIC](https://neic.nordforsk.org)


## Recommendation

- Write me ...


## Background

Currently the code is hosted on [GitLab.com](https://gitlab.com) (GitLab.com is
a service and company, GitLab is a software) A repository hosting platform not
only hosts the Git repository but also provides an issue tracker, user access
management, wiki, hooks and integrations with other services, as well as a
solution for code review. The choice of a repository hosting platform can
affect the workflow.


## Option 1.1: [GitHub](https://github.com)

### Strengths

- The currently largest repository hosting platform with millions of active projects and users
- Stability
- High availability
- Responsiveness
- Visibility
- Integration with [Travis CI](https://travis-ci.org) for automated tests


### Weaknesses

- Underlying source code powering the [GitHub](https://github.com) platform is not open source
- Private repositories cost money


### Risks

- Many Dalton developers may not be familiar with it (but the workflow is very similar to [GitLab.com](https://gitlab.com))
- Lock-in through integrations and issue tracking and code review discussions


### Opportunities

- Clean-slate opportunity to prevent direct pushes to the main development line and to implement code review from day 1
- Forks of the public repository are more likely to be public


## Option 1.2: [GitLab.com](https://gitlab.com)

### Strengths

- Private repositories are free
- Underlying source code powering the community edition of [GitLab](https://gitlab.com) is open source
- Dalton developers are familiar with the platform
- More flexibility to connect repositories with own test runners


### Weaknesses

- Slightly less responsive and available than [GitHub](https://github.com)
- Less visibility than [GitHub](https://github.com)


### Risks

- High inertia to implement code review and prevent direct pushed to the main development line.
  This is because we are used to a workflow _without_ code review on this platform.
- Forks of the public repository may remain private, with potential loss of contributions.


### Opportunities

- Easier to migrate data to an own instance


## Option 1.3: Nordic-wide GitLab server deployed by [NeIC](https://neic.nordforsk.org)

### Strengths

- Full control over data
- Data stays within the Nordics
- Commitment to a service


### Weaknesses

- Not in place yet
- Will probably be available as beta in April or May
- Less availability (much smaller support team) than Option 1.2


### Risks

- Long-term future is not clear but NeIC is working on a long-term sustainability of this service


### Opportunities

- Politically good solution to reinforce NeIC initiatives and services


# 2) Decision about which branches should be part of the public repository


## Options

- Option 2.1: We only open the master branch and past release branches
- Option 2.2: We open all branches by default but give developers enough time to mark those branches which should not be opened


## Recommendation

- RDR: Open up master and all past releases. Rationale: In the workflow we want
  to move to the public repository only has a handful of stable branches.
  I would urge to follow the example of the Psi4 community, where all development runs on forks.


## Background

In Dalton we have 236 branches. Most of these are unmaintained. In LSDalton we
have 81 branches. We will open the entire history of at least the `master`
branch and past release branches but we can also open additional branches.


## Option 2.1: We only open the master branch and past release branches

### Strengths

- Simple, clear, and clean


### Weaknesses

- RDR. If we go for an interim period of private/public coexistence, we might
  end up with a dead public repo and a struggling private repo.


### Risks

- Since the public repository is supposed to be the main repository we risk
  leaving many branches behind
- Some developers may regard the public repository as a mirror and continue
  contributing to the `master` branch on the private repo which would let both
  repositories diverge


### Opportunities

- Getting rid of many unmaintained branches


## Option 2.2: We open all branches by default but give developers enough time to mark those branches which should not be opened

### Strengths

- No unmaintained branch left behind


### Weaknesses

- Main repository cluttered with tens and hundreds of branches


### Risks

- Some developers may forget branches and accidentally open them


### Opportunities

- Less risk that the public repository is not accepted as the main development line


# 3) Decision about the timeline for opening the public repository

## Options

- Option 3.1: Make the public repository available as soon as technically possible
- Option 3.2: Make the public repository available as soon as we release a new Dalton/LSDalton version


## Recommendation

- RDR. With the _caveat_ that the recommendation in point 2 is followed, open
  up as soon as technically possible. Rationale: leverage the momentum of the
  open sourcing decision to bring the process to completion. In addition, it is
  not needed to have a release to publish past releases.

### Notes

- RDR. A simplification of the release process is sorely needed.

## Background

As soon as we make the public repository available, users can start cloning the
code and using the `master` branch or any of the release branches and
developers can start building based on our code by forking the repository. The
opening of the repository is in principle independent of an official Dalton
release. The opening can be synchronized with a new official release of the
code. In any outcome we will make explicitly clear where the released versions
reside, how to reproduce them (they may require a "make release" step) and that
the `master` branch may contain experimental and untested and unreleased
functionality.


## Option 3.1: Make the public repository available as soon as technically possible

### Strengths

- The opening does not get delayed if the release gets delayed
- Transparent release preparation


### Weaknesses

- Requires clear documentation and communication of scope and intent to avoid confusion.
  That is, we want to avoid people thinking that `master` is the next stable release.


### Risks

- Users may misunderstand this step as releasing a new version


### Opportunities

- We open the code sooner and reduce the "time to market"
- Users have the chance to report issues during release preparation


## Option 3.2: Make the public repository available as soon as we release a new Dalton/LSDalton version

### Strengths

- More visibility: use the momentum of a new release to advertise the new licensing and the open repository


### Weaknesses

- Non-transparent release preparation


### Risks

- Opening risks to get delayed with a delayed release
- No opportunity to report issues during release preparation


### Opportunities

- ?


# 4) Decision about the contribution model for the public repository


## Options

- Option 4.1: All developers can push (present model)
- Option 4.2: Nobody pushes directly, all contributions are integrated via pull-requests or merge-requests


## Recommendation

- RDR. The main development line should be public _and_ protected. Git branch
  protection, CI and code review are a huge initial investment, but the payoff
  in the medium-long run largely offsets these.

# 5) CI Service

## Background

The foundations for a well-functioning fork/pull-request workflow are:
1. A CI service that checks that each potential commit to the mainline isn't breaking
2. Code review that ensures each potential commit follows contribution guidelines to the project.

In light of these two points, choosing a CI service is as essential as choosing the code hosting platform.

**CI services cost money**

The choice of CI service strongly depends on two factors:
- The code hosting platform
- The availability of funds

## Options

- Option 5.1: Self-hosted
- Option 5.2: Cloud-based using Travis CI
- Option 5.3: Cloud-based using GitLab CI

## Recommendation

1. If the choice is made to go on GitHub, adopt Travis CI, but investigate
   self-hosting services like Drone CI
   - Travis is free for open source projects
2. If the choice is made to stay on GitLab, adopt GitLab CI, but investigate
   self-hosting services like Drone CI
   - GitLab offers shared runners for free, I am not sure of their reliability
   - Self-hosting is already possible with GitLab CI


## Option 5.1: Self-hosted

### Strengths

- Cloud-based virtual machines are the best option, since they limit potential security risks.
- No run time limitations
- Testing with commercial compilers and libraries possible (without breaching licenses)

### Weaknesses

- Who will front these expenses?

### Risks

- Outages on the cloud are beyond anyone's control

### Opportunities

- Self-hosting CI services have been around for a while, we can surely find
  something that suits this project.

## Option 5.2: Cloud-based using Travis CI

### Strengths

- Cloud-based, but managed by somebody else
- Free for open source projects on GitHub
- Testing on Linux and Mac OS X

### Weaknesses

- The amount of concurrent builds that can be run is limited on the free plan
- The amount of time per build is limited on the free plan
- The build images one can use are limited and might contain outdated dependencies
- Mac OS X builds have been erratic in the past few months

### Risks

- Outages on Travis are not unknown of, but almost always they happen for Mac OS X
- Tighter integration and lock-in effect with GitHub

### Opportunities

- Write me...

## Option 5.3: Cloud-based using GitLab CI

### Strengths

- Cloud-based, either managed by somebody else or self-hosted
- Superb integration with Docker. One can super-charge the build environment with all necessary dependencies to speed up builds.
- Free, using shared runners, for all projects, public and private

### Weaknesses

- The shared runners do not seem to be highly reliable

### Risks

- GitLab.com might decide to pull the plug on the free shared runners (unlikely)
- Tighter integration and lock-in effect with GitLab.com

### Opportunities

- GitLab-CI can be self-hosted, protecting us somewhat from the unreliability of shared runners.
